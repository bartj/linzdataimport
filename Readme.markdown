# LINZ Title / Owner Data Importer #

http://data.linz.govt.nz/ provides New Zealand title and owner data free of charge. Since the CSV file is 1.9 GB, I wanted to load it into a database for indexing and quick processing. This project does that and not much more - do not expect programming perfection!