﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Reactive.Linq;

namespace TitleOwnerImporter
{
    sealed class BulkSqlSaver : IDisposable
    {
        readonly IVerboseLogger _logger;
        IDisposable _subscription;
        readonly SqlConnection _connection;
        const string DestinationTableName = "Owner";

        public BulkSqlSaver(IVerboseLogger logger)
        {
            _logger = logger;
            _connection = new SqlConnection("Server=(local);Database=LinzData;Trusted_Connection=True;");
            _connection.Open();

            ClearTable();
        }

        void ClearTable()
        {
            using (var cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "truncate table " + DestinationTableName;
                cmd.ExecuteNonQuery();
            }
        }

        public void Save(IObservable<PropertyDetails> detailsObservable)
        {
            _subscription = detailsObservable.Buffer(1000).Subscribe(ProcessNewPropertyDetails);
        }

        void ProcessNewPropertyDetails(IList<PropertyDetails> details)
        {
            _logger.Log(string.Format(CultureInfo.CurrentCulture, "Processing {0} records...", details.Count));
            
            var copier = new SqlBulkCopy(_connection);
            copier.DestinationTableName = DestinationTableName;
            copier.BatchSize = details.Count;
            copier.ColumnMappings.Add("PropertyId", "PropertyId");
            copier.ColumnMappings.Add("Name", "Name");
            copier.WriteToServer(new PropertyDetailsDataReader(details, new[] { "PropertyId", "Name" }));
        }

        public void Dispose()
        {
            _subscription.Dispose();
            _connection.Dispose();
        }
    }
}