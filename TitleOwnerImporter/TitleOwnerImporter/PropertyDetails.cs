﻿namespace TitleOwnerImporter
{
    class PropertyDetails
    {
        public int PropertyId { get; private set; }
        public string Name { get; private set; }

        public PropertyDetails(int propertyId, string name)
        {
            PropertyId = propertyId;
            Name = name;
        }
    }
}