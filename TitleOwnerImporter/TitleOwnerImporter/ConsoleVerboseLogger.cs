﻿using System;

namespace TitleOwnerImporter
{
    public class ConsoleVerboseLogger : IVerboseLogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}