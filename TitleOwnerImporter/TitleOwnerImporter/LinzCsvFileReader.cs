﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;

namespace TitleOwnerImporter
{
    class LinzCsvFileReader
    {
        readonly string _inputFilePath;

        public LinzCsvFileReader(string inputFilePath)
        {
            _inputFilePath = inputFilePath;
        }

        public IEnumerable<PropertyDetails> ReadPropertyDetails()
        {
            using (var file = File.OpenText(_inputFilePath))
            using (var reader = new CsvReader(file))
            {
                foreach(var value in reader.GetRecords<CsvLine>().SelectMany(ConvertCsvLineToPropertyDetails))
                    yield return value;
            }
        }

        static IEnumerable<PropertyDetails> ConvertCsvLineToPropertyDetails(CsvLine line)
        {
            int id = line.id;
            string[] owners = line.owners.Split(',').Select(s => s.Trim()).ToArray();
            return owners.Select(owner => new PropertyDetails(id, owner));
        }

        class CsvLine
        {
            public int id { get; set; }
            public string title_no { get; set; }
            public string status { get; set; }
            public string type { get; set; }
            public string land_district { get; set; }
            public string issue_date { get; set; }
            public string guarantee_status { get; set; }
            public string estate_description { get; set; }
            public string owners { get; set; }
            public string spatial_extents_shared { get; set; }
            public string WKT { get; set; }
        }
    }
}