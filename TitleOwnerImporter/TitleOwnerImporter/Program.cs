﻿using System;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Linq;

namespace TitleOwnerImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new CommandLineOptions();
            if (CommandLine.CommandLineParser.Default.ParseArguments(args, options))
            {
                var logger = options.Quiet ? new NullVerboseLogger() : (IVerboseLogger)new ConsoleVerboseLogger();
                var csvFileReader = new LinzCsvFileReader(options.InputFile);
                var detailsObservable = csvFileReader.ReadPropertyDetails().ToObservable();

                using (var saver = new BulkSqlSaver(logger))
                {
                    saver.Save(detailsObservable);
                }
            }
        }
    }
}
