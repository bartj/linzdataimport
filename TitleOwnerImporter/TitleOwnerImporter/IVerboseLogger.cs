﻿namespace TitleOwnerImporter
{
    public interface IVerboseLogger
    {
        void Log(string message);
    }
}