﻿using CommandLine;
using CommandLine.Text;

namespace TitleOwnerImporter
{
    class CommandLineOptions : CommandLineOptionsBase
    {
        [Option("r", "read", Required = true, HelpText = "Input file to be processed.")]
        public string InputFile { get; set; }

        [Option("q", "quiet", DefaultValue = false, HelpText = "Only display error output.")]
        public bool Quiet { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}